﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace firestore.common
{
    public class AnnotationHelper
    {
        public static string CreateCsvHeaderLine<T>(Type target)
        {
            PropertyInfo[] properties = target.GetProperties();
            List<string> propertyValues = new List<string>();

            foreach (var prop in properties)
            {
                var ignoreAttribute = prop.GetCustomAttribute(typeof(JsonIgnoreAttribute));
                if (ignoreAttribute != null)
                {
                    continue;
                }
                string stringformatString = string.Empty;
                string value = prop.Name;

                var attribute = prop.GetCustomAttribute(typeof(JsonPropertyAttribute));
                if (attribute != null)
                {
                    value = (attribute as JsonPropertyAttribute).PropertyName;
                }
            }

            return "";
        }
    }
}
