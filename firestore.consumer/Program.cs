﻿using firestore.entity;
using repository.firestore;
using System;
using System.Threading.Tasks;

namespace firestore.consumer
{
    class Program
    {
        static async Task Main(string[] args)
        {
            ClientRepository repo = new ClientRepository();
            //Client clt = await repo.Get(Guid.Parse("361c5f42-1cbe-4c55-980b-17906fa05cf7"));

            Guid id = Guid.NewGuid();
            var client = new Client
            {
                Id = 1,
                Name = "Name: " + id.ToString(),
                IsAdmin = true,
                Age = 31,
                Values = new System.Collections.Generic.List<Values> { new Values { code = "1", text = "Hello" } }
            };

            await repo.Save(client);
            //client.Name = "New Name";

            //await repo.Save(client);
            //var clients = await repo.GetAll(p => p.Name.Contains("New"));
            var clients = await repo.GetAll();
            Console.Write(clients);
        }
    }
}
