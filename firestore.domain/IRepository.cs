﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace firestore.domain
{
    public interface IRepository<T, TId>
    {
        Task<List<T>> GetAll(Expression<Func<T, bool>> predicate = null);
        Task<T> Get(TId id);
        Task<bool> Save(T entity);
    }
}
