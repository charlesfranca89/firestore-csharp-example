﻿using Google.Cloud.Firestore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace firestore.entity
{
    [FirestoreData]
    public class Client
    {
        [Key]
        [FirestoreProperty]
        public int Id { get; set; }

        [JsonProperty("UserName")]
        [FirestoreProperty]
        public string Name { get; set; }

        [FirestoreProperty(ConverterType = typeof(ValuesConverter))]
        public List<Values> Values { get; set; }

        [FirestoreProperty]
        public bool IsAdmin { get; set; }

        [FirestoreProperty]
        public int Age { get; set; }
    }

    [FirestoreData]
    public class Values
    {
        [FirestoreProperty]
        public string text { get; set; }

        [FirestoreProperty]
        public string code { get; set; }
    }

    public class ValuesConverter : IFirestoreConverter<Values>
    {
        // A PlayerId should be represented in storage as just the string value.
        public object ToFirestore(Values value) {
            return (dynamic)value;
        }

        public Values FromFirestore(object value)
        {
            var jsonString = JsonConvert.SerializeObject(value);
            return JsonConvert.DeserializeObject<Values>(jsonString);
        }
    }
}
