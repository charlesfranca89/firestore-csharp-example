﻿using Google.Cloud.Firestore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace firestore.helper
{
    public class FirestoreHelper
    {
        private static FirestoreDb db;
        public static void Config(string authJsonPAth, string projectId)
        {
            Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", authJsonPAth);
            db = FirestoreDb.Create(projectId);
        }
        public static async Task<bool> SaveOrUpdateFirebaseObject(string collection, object target)
        {
            var valuePairs = GetKeyValuePairs(target);
            DocumentReference docRef = db.Collection(collection).Document(valuePairs.FirstOrDefault(p => p.IsKey).Value.ToString());
            Dictionary<string, object> targetValues = new Dictionary<string, object>();
            foreach (var item in valuePairs)
            {
                targetValues.Add(item.DisplayName, item.Value);
            }
            await docRef.SetAsync(targetValues);
            return true;
        }
        public static async Task<List<T>> GetList<T>(string collection, Expression<Func<T, bool>> predicate = null)
        {
            List<T> objects = new List<T>();
            List<FirestoreProp> firestoreProps = GetProps(typeof(T));

            CollectionReference usersRef = null;
            if (predicate != null)
            {
                BinaryExpression be = predicate.Body as BinaryExpression;
                Query capitalQuery = null;

                int indexOfDot = be.Left.ToString().IndexOf('.') + 1;
                string propertyName = be.Left.ToString().Substring(indexOfDot, be.Left.ToString().Length - indexOfDot);
                object propertyValue = be.Right;
                if (propertyValue.ToString().Contains("Parse"))
                {
                    propertyValue = propertyValue.ToString().Replace("Parse(\"", "");
                    propertyValue = propertyValue.ToString().Replace("\")", "");
                }

                if (predicate.Body.NodeType == ExpressionType.Equal)
                {
                    capitalQuery = db.Collection(collection).WhereEqualTo(propertyName, propertyValue);
                }

                if (predicate.Body.NodeType == ExpressionType.GreaterThan)
                {
                    capitalQuery = db.Collection(collection).WhereGreaterThan(propertyName, propertyValue);
                }

                if (capitalQuery == null)
                {
                    throw new NotImplementedException();
                }

                QuerySnapshot capitalQuerySnapshot = await capitalQuery.GetSnapshotAsync();
                foreach (DocumentSnapshot documentSnapshot in capitalQuerySnapshot.Documents)
                {
                    Dictionary<string, object> docResponse = documentSnapshot.ToDictionary();
                    var targetType = (T)Activator.CreateInstance(typeof(T), new object[] { });

                    foreach (var p in firestoreProps)
                    {
                        PropertyInfo pinfo = targetType.GetType().GetProperty(p.Name);
                        if (p.PropType == typeof(Guid))
                        {
                            pinfo.SetValue(targetType, Guid.Parse(docResponse[p.DisplayName].ToString()), null);
                        }
                        else
                        {
                            pinfo.SetValue(targetType, docResponse[p.DisplayName].ToString(), null);
                        }

                    }
                    objects.Add(targetType);
                }

                return objects;
            } else
            {
                usersRef = db.Collection(collection);
            }

            QuerySnapshot snapshot = await usersRef.GetSnapshotAsync();
            var primitiveTypes = new List<Type> { typeof(int), typeof(string), typeof(bool) };
            foreach (DocumentSnapshot document in snapshot.Documents)
            {
                Dictionary<string, object> documentDictionary = document.ToDictionary();
                var targetType = (T)Activator.CreateInstance(typeof(T), new object[] { });

                foreach (var p in firestoreProps)
                {
                    PropertyInfo pinfo = targetType.GetType().GetProperty(p.Name);
                    if (p.PropType == typeof(Guid))
                    {
                        pinfo.SetValue(targetType, Guid.Parse(documentDictionary[p.DisplayName].ToString()), null);
                    }
                    else if(primitiveTypes.Contains(p.PropType))
                    {
                        pinfo.SetValue(targetType, Convert.ChangeType(documentDictionary[p.DisplayName], p.PropType), null);
                    } else
                    {
                        var objectString = JsonConvert.SerializeObject(documentDictionary[p.DisplayName]);
                        pinfo.SetValue(targetType, JsonConvert.DeserializeObject(objectString, p.PropType), null);
                    }

                }

                objects.Add(targetType);
            }
            return objects;
        }
        public static async Task<T> Get<T>(string collection, object key)
        {
            var targetType = (T)Activator.CreateInstance(typeof(T), new object[] { });
            List<FirestoreProp> firestoreProps = GetProps(typeof(T));

            DocumentReference docRef = db.Collection(collection).Document(key.ToString());
            DocumentSnapshot snapshot = await docRef.GetSnapshotAsync();

            if (snapshot.Exists)
            {
                Console.WriteLine("Document data for {0} document:", snapshot.Id);
                Dictionary<string, object> documentValue = snapshot.ToDictionary();
                foreach (var p in firestoreProps)
                {
                    PropertyInfo pinfo = targetType.GetType().GetProperty(p.Name);
                    if (p.PropType == typeof(Guid))
                    {
                        pinfo.SetValue(targetType, Guid.Parse(documentValue[p.DisplayName].ToString()), null);
                    }
                    else
                    {
                        pinfo.SetValue(targetType, documentValue[p.DisplayName].ToString(), null);
                    }

                }
            }
            else
            {
                Console.WriteLine("Document {0} does not exist!", snapshot.Id);
            }
            return targetType;
        }
        private static List<FirestoreProp> GetKeyValuePairs(object src)
        {
            List<FirestoreProp> firestoreProps = GetProps(src.GetType());
            foreach (var prop in firestoreProps)
            {
                var propValue = GetPropValue(src, prop.Name);
                if (propValue is Guid)
                {
                    propValue = propValue.ToString();
                }
                prop.Value = propValue;
            }
            return firestoreProps;
        }

        private static List<FirestoreProp> GetProps(Type srcType)
        {
            List<FirestoreProp> firestoreProps = new List<FirestoreProp>();

            Type type = srcType;
            PropertyInfo[] properties = type.GetProperties();

            foreach (var prop in properties)
            {
                var ignoreAttribute = prop.GetCustomAttribute(typeof(JsonIgnoreAttribute));
                if (ignoreAttribute != null)
                {
                    continue;
                }

                string value = prop.Name;
                var attribute = prop.GetCustomAttribute(typeof(JsonPropertyAttribute));
                if (attribute != null)
                {
                    value = (attribute as JsonPropertyAttribute).PropertyName;
                }

                var isKey = false;
                var keyAttr = prop.GetCustomAttribute(typeof(KeyAttribute));
                if (keyAttr != null)
                {
                    isKey = true;
                }

                firestoreProps.Add(new FirestoreProp
                {
                    DisplayName = value,
                    Name = prop.Name,
                    IsKey = isKey,
                    PropType = prop.PropertyType
                });
            }
            return firestoreProps;
        }
        private static object GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }
    }
}
