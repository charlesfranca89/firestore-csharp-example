﻿using System;
using System.Collections.Generic;
using System.Text;

namespace firestore.helper
{
    public class FirestoreProp
    {
        public string Name { get; set; }
        public object Value { get; set; }
        public bool IsKey { get; set; }
        public string DisplayName { get; set; }
        public Type PropType { get; set; }
    }
}
