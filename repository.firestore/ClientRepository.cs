﻿using firestore.domain;
using firestore.entity;
using firestore.helper;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace repository.firestore
{
    public class ClientRepository : IRepository<Client, Guid>
    {
        public ClientRepository()
        {
            FirestoreHelper.Config("firebase_auth.json", "movies-app-b5466");
        }

        public async Task<bool> Save(Client entity)
        {
            await FirestoreHelper.SaveOrUpdateFirebaseObject("users", entity);
            return true;
        }

        public async Task<Client> Get(Guid id)
        {
            return await FirestoreHelper.Get<Client>("users", id);
        }

        public async Task<List<Client>> GetAll(Expression<Func<Client, bool>> predicate = null)
        {
            return await FirestoreHelper.GetList<Client>("users", predicate);
        }
    }
}
